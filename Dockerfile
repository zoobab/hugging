FROM alpine:3.17
RUN apk add git git-lfs
RUN git lfs install
RUN export GIT_LFS_SKIP_SMUDGE=1
RUN git clone --depth=1 https://huggingface.co/setu4993/smaller-LaBSE && cd smaller-LaBSE && git lfs pull && rm -Rfv .git/
